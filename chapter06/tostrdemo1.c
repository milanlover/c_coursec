#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char const *argv[])
{
    char arr[] = "https://www.simoniu.com"; //字符数组
    char *str = arr;
    char *str1 = "https://www.simoniu.com"; //字符串常量
    char *str2 = str1;

    for (int i = 0; i < strlen(arr); i++)
    {
        printf("%c", *str);
        str++;
    }
    printf("\n---------------------\n");

    for (int i = 0;; i++)
    {
        if (*str1 == '\0')
        {
            break;
        }
        printf("%c", *str1);
        str1++;
    }
    printf("\n---------------------\n");
    printf("%s\n", str);
    printf("%s\n", str2);
    puts(str2);
    return 0;
}
