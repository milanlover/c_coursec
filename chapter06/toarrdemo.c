#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const *argv[])
{
    int arr[] = {99, 15, 100, 888, 252};
    int *p = arr;

    printf("数组第一个元素的地址：%d\n", &arr[0]);
    printf("数组的首地址：%d\n", &arr);
    printf("指针变量p的值：%d\n", p);

    return 0;
}
