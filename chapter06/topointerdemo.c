#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char const *argv[])
{
    int x = 100;
    int *p = &x;
    int **pt = &p;

    printf("pt指向的指针所指向的内存单元的值是：%d\n", **pt);
    printf("p指向的内存单元的值是：%d\n", *p);
    printf("x的值是：%d\n", x);

    printf("pt的值是：%d\n", pt);
    printf("p的地址是：%d\n", &p);
    return 0;
}
