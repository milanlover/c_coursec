#include <stdio.h>
#include <stdlib.h>
#define LEN(x) sizeof(x) / sizeof(x[0])

int main(int argc, char const *argv[])
{
    int arr[] = {99, 15, 100, 888, 252};
    int *p = arr;
    int len = LEN(arr);
    /*方式一
    for(int i=0;i<len;i++){
       printf("%d ",*(p+i));
    }*/
    //方式二
    for (int i = 0; i < len; i++)
    {
        printf("%d ", *p);
        p++;
    }
    return 0;
}
