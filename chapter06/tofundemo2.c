#include <stdio.h>
#include <stdlib.h>
#include <string.h>

double add(int x, int y)
{
    return x + y;
}

double sub(int x, int y)
{
    return x - y;
}

double multi(int x, int y)
{
    return x * y;
}

double divid(int x, int y)
{
    if (y == 0)
    {
        return 0.0;
    }
    else
    {
        return x * 1.0 / y;
    }
}

int main(int argc, char const *argv[])
{
    // 1.定义函数指针
    double (*p)(int x, int y);
    // 2.显示菜单
    int x, y, choice;
    printf("请输入两个整数：\n");
    scanf("%d", &x);
    scanf("%d", &y);
    printf("请选择操作(1.加法 2.减法 3.乘法  4.除法)：\n");
    scanf("%d", &choice);

    switch (choice)
    {
    case 1:
        p = add;
        break;
    case 2:
        p = sub;
        break;
    case 3:
        p = multi;
        break;
    case 4:
        p = divid;
        break;
    }
    int result = p(x, y);
    printf("the result is:%d\n", result);
    return 0;
}
