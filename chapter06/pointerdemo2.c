#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const *argv[])
{
    int x = 100;
    int *p = &x; // p称为指针变量，p里面保存的值就是指针

    printf("指针变量p里面保存的值：%d\n", p);
    printf("x的地址值是：%d\n", &x);
    printf("指针变量p里面保存的值：%d\n", &*p);

    printf("指针变量p指向的内存单元的值：%d\n", *p);
    printf("x的值是：%d\n", x);
    printf("指针变量p指向的内存单元的值：%d\n", *&x);

    printf("指针变量p的地址是：%d\n", &p);
    return 0;
}
