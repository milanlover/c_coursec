#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int add(int x, int y)
{
    return x + y;
}

int main(int argc, char const *argv[])
{
    int x = 10;
    int y = 5;
    int (*fn)(int, int) = add;

    printf("x+y=%d\n", add(x, y));
    printf("x+y=%d\n", fn(x, y));
    printf("x+y=%d\n", (*fn)(x, y));
    return 0;
}
