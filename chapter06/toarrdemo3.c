#include <stdio.h>
#include <stdlib.h>
#define LEN(x) sizeof(x) / sizeof(x[0])

int main(int argc, char const *argv[])
{
    int arr[] = {99, 15, 100, 888, 252};
    int *p = arr;
    int len = LEN(arr);

    for (int i = 0; i < len; i++)
    {
        printf("%d ", *(arr + i)); // ok.
    }
    /*
    for(int i=0;i<len;i++){
       printf("%d ",*p);
       arr++;	//错误,arr是指针常量
     }*/
    return 0;
}