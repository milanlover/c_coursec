#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char const *argv[])
{
    char arr[] = "https://www.simoniu.com";

    char *str1 = "https://www.simoniu.com"; // str1是字符串常量。

    arr[0] = 'H';
    // str1[0]='H'; //错误，因为str1是字符串常量，只能读取不能写入。
    puts(arr);

    puts(str1);
    // arr="hello,world"; //只能在定义的同时初始化。
    str1 = "hello,world!"; // str1指向另一个字符串常量。
    // str1[0]='H'; //错误。
    puts(str1);

    str1 = arr;
    str1[0] = 'H'; // ok,因为此时str1指向的是字符串变量。
    puts(str1);
    return 0;
}
