//预处理指令实例
#include <stdio.h>
//不同平台，引入不同的头文件
#if _WIN32
#include <windows.h>
#elif _linux_
#include <unistd.h>
#endif

int main(int argc, char const *argv[])
{
//不同平台下调用不同的函数
#if _WIN32
    Sleep(5000);
#elif _linux_
    sleep(5);
#endif

    puts("hello world!\n");
    return 0;
}
