#include <stdio.h>
#define PI 3.14159
#define MAX(a, b) ((a > b) ? a : b)

double getCircle(double r)
{

    return 2 * PI * r;
}

double getArea(double r)
{
    return PI * r * r;
}

int main(int argc, char const *argv[])
{
    double r = 10.0; //半径

    int x, y, max;

    printf("圆的周长是：%lf\n", getCircle(r));
    printf("圆的面积是：%lf\n", getArea(r));

    printf("input two numbers:\n");
    scanf("%d %d", &x, &y);
    max = MAX(x, y);
    printf("max=%d\n", max);
    return 0;
}
