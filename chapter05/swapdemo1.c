#include <stdio.h>
#include <stdlib.h>

void swap(int x, int y)
{
    int temp;
    temp = x;
    x = y;
    y = temp;

    printf("in swap function x=%d,y=%d\n", x, y);
}

int main(int argc, char const *argv[])
{
    int x = 10;
    int y = 7;
    swap(x, y);
    printf("x=%d,y=%d", x, y);
    return 0;
}
