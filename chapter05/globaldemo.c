#include <stdio.h>
#include <stdlib.h>

int x = 10;

/*
void fn()
{
    int x = 100;
    printf("x=%d\n", x);
}*/

//n函数可以直接访问全局变量x.
void fn(){
   x++;
   printf("x=%d\n",x);
}

int main(int argc, char const *argv[])
{
    fn();
    printf("x=%d\n", x); //输出全局变量x
    return 0;
}
