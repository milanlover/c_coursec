#include <stdio.h>
#include <stdlib.h>

//这里是函数的声明

int add(int, int);

int main(int argc, char const *argv[])
{
    int x = 10;
    int y = 7;
    printf("%d", add(x, y)); //这里的x,y是实参
    return 0;
}

//这里是add函数的定义
int add(int x, int y)
{
    return x + y;
}
