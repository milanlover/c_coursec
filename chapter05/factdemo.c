#include <stdio.h>
#include <stdlib.h>

long long fact(long long n, long long prev, long long next)
{

    if (n == 2)
    {
        return next;
    }
    return fact(n - 1, next, prev + next);
}

int main(int argc, char const *argv[])
{
    long long n = 100;
    printf("第%d项是：%lld\n", n, fact(n, 1, 1));
    return 0;
}
