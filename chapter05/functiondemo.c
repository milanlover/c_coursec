#include <stdio.h>
#include <stdlib.h>

int add(int x, int y)
{
    return x + y;
}

int main(int argc, char const *argv[])
{
    int x = 10;
    int y = 7;
    int z = add(x, y); //这里是函数调用
    printf("z=%d", z);
    return 0;
}
