#include <stdio.h>
#include <stdlib.h>

void fn()
{
    extern int x;
    x = 100;
    printf("x=%d\n", x);
}

int x = 10;

int main(int argc, char const *argv[])
{
    fn();
    printf("x=%d\n", x); //输出全局变量x
    return 0;
}
