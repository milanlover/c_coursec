#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const *argv[])
{
    const int x = 100;
    printf("x=%d\n", x++); //错误，常量不能改变。
    return 0;
}
