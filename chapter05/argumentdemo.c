#include <stdio.h>
#include <stdlib.h>

//这里的x,y是形参
int add(int x, int y)
{
    return x + y;
}

int main(int argc, char const *argv[])
{
    int x = 10;
    int y = 7;

    printf("%d", add(x, y)); //这里的x,y是实参
    return 0;
}
