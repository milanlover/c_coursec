#include <stdio.h>
#include <stdlib.h>
#define LEN(x) sizeof(x) / sizeof(x[0])

int search(int arr[], int len, int dest)
{
    int iCount = 0;
    for (int i = 0; i < len; i++, iCount++)
    {
        if (arr[i] == dest)
        {
            printf("找到元素，i=%d\n", i);
            printf("循环次数：%d\n", iCount);
            return i;
        }
    }
    printf("循环次数：%d\n", iCount);
    return -1;
}

int main(int argc, char const *argv[])
{
    int arr[] = {23, 14, 8, 10, 18, 9, 33, 21, 17, 19, 4, 32};
    int dest = 4;
    int pos = -1;

    if ((pos = search(arr, LEN(arr), dest)) != -1)
    {
        printf("索引为%d的位置找到元素：%d\n", pos, dest);
    }
    else
    {
        printf("未找到元素%d\n", dest);
    }
    return 0;
}
