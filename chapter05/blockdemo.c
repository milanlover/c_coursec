#include <stdio.h>
#include <stdlib.h>

int x = 10;
void fn()
{
    int x = 100;
    printf("x=%d\n", x);
}

int main(int argc, char const *argv[])
{
    fn();
    {
        int x = 1000;
        printf("x=%d\n", x); //输出代码块变量x
    }
    printf("x=%d\n", x); //输出全局变量x
    return 0;
}
