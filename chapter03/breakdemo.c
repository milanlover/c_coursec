#include <math.h>
#include <stdio.h>
#include <string.h>

int main(int argc, char const *argv[])
{
    int num;
    printf("请输入一个正整数：\n");
    scanf("%d", &num);

    int flag = 1; //默认是素数
    for (int i = 2; i <= sqrt(num); i++)
    {
        if (num % i == 0)
        {
            flag = 0;
            break; //已经不是素数了则跳出循环。
        }
    }

    if (flag)
    {
        printf("num= %d是素数！", num);
    }
    else
    {
        printf("num= %d不是素数！", num);
    }
    return 0;
}
