#include <stdio.h>
#include <string.h>

int main(int argc, char const *argv[])
{
    int score;
    printf("%s\n", "请输入考试成绩：");
    scanf("%d", &score);

    /*
    //简单if语句
    if(score==100){
        printf("%s","奖励变形金刚一个！");
    } */

    /*
    //if-else 语句
    if (score > 100 || score < 0)
    {
        printf("成绩非法!");
        return -1;
    }

    if (score >= 60)
    {
        printf("%s", "恭喜你考试及格！");
    }
    else
    {
        printf("%s", "暴打一顿！");
    }*/


    /*
    //多重if-else语句
    if (score > 100 || score < 0)
    {
        printf("成绩非法!");
        return -1;
    }

    if (score >= 90 && score <= 100)
    {
        printf("%s", "成绩优秀！");
    }
    else if (score >= 80 && score < 90)
    {
        printf("%s", "成绩良好！");
    }
    else if (score >= 60 && score < 80)
    {
        printf("成绩及格!");
    }
    else
    {
        printf("不及格！");
    }
    */

    return 0;
}
