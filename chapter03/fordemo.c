#include <stdio.h>
#include <string.h>

int main(int argc, char const *argv[])
{
    int sum=0;
    for (int n = 1; n <= 100; n++)
    {
        sum += n;
    }
    printf("sum=%d\n", sum);
    return 0;
}
