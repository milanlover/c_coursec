#include <math.h>
#include <stdio.h>
#include <string.h>

int main(int argc, char const *argv[])
{
    int age;
    while (1)
    {
        printf("请输入玩家的年龄：\n");
        scanf("%d", &age);

        if (age < 0 || age > 200)
        { //认为年龄非法
            printf("您输入的年龄不合法！\n");
            continue;
        }
        printf("玩家的年龄是：%d", age);
        break;
    }
    return 0;
}
