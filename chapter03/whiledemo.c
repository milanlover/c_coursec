#include <stdio.h>
#include <string.h>

int main(int argc, char const *argv[])
{
    int n = 1;
    int sum = 0;
    while (n <= 100)
    {
        sum += n;
        n++;
    }
    printf("sum=%d\n", sum);
    return 0;
}
