#include <stdio.h>
#include <string.h>

//嵌套if-else语句案例

int main(int argc, char const *argv[])
{
    char gender[10];
    int height;
    printf("请输入性别：");
    scanf("%s", &gender);
    printf("请输入身高(cm)：");
    scanf("%d", &height);

    if (strcmp(gender, "男") == 0)
    {
        if (height >= 180)
        {
            printf("恭喜您，入选校男子篮球队！");
        }
        else
        {
            printf("很遗憾您未能入选校男子篮球队！");
        }
    }
    else
    {
        if (height >= 170)
        {
            printf("恭喜您，入选校女子篮球队！");
        }
        else
        {
            printf("很遗憾您未能入选校女子篮球队！");
        }
    }
    return 0;
}
