#include <stdio.h>
#include <string.h>

int main(int argc, char const *argv[])
{
    int score;
    printf("%s\n", "请输入考试成绩：");
    scanf("%d", &score);

    if (score > 100 || score < 0)
    {
        printf("成绩非法!");
        return -1;
    }
    int level = score / 10;

    switch (level)
    {
    case 10:
    case 9:
        printf("成绩优秀！");
        break;
    case 8:
        printf("成绩良好！");
        break;
    case 7:
    case 6:
        printf("成绩及格！");
        break;
    default:
        printf("不及格！");
        break;
    }
    return 0;
}
