/*
字符类型
*/

#include <stdio.h>
int main(int argc, char const *argv[])
{
    //1.定义字符变量
    char c1 = 'A';
    //char c2 = '中'; //错误
    //char c3 = "A"; //错误
    //char c4 = 'Ａ';  //错误，A 是一个全角字符 
    char c5 = ' ';  // 空格也是一个字符
    
    putchar(c1);
    putchar('\n');
    printf("%c\n",c1);  


    //2.char类型与int类型转换
    char c = 'A';
    int n = 65;
    char c3 = ++n;
    
    printf("%c\n",c); 
    printf("%d\n",(int)c);
    printf("%c\n",(char)n);
    printf("%c\n",c3); 


    return 0;
}
