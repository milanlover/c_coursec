/*
字符串
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char const *argv[])
{
    //1.如何定义字符串变量
    /*
    char* str1 = "I love you china!";
    char* str2 = "hello,一起学习C语言吧！";
    */

    //也可以使用char[]方式定义字符串变量。
    char str1[50] = "I love you china!";
    char str2[50] = "hello,一起学习C语言吧！";
    
    printf("%s\n",str1);
    printf("%s\n",str2);

    //2.字符串长度

    printf("str1 length is :%d\n",strlen(str1));
    printf("str2 length is :%d\n",strlen(str2));
    
    return 0;
}
