/*
unsigned 无符号
*/

#include <stdio.h>

int main(int argc, char const *argv[])
{
    unsigned short a = 12;
    unsigned int b = 1002;
    unsigned long c = 9892320;

    // int n = 2147483647;
    // n赋值给更大正整数，则会出现n=-1;
    // int n = 4294967295;
    unsigned int n = 4294967295UL;

    // printf("n=%d",n);
    printf("n=%u", n);
    return 0;
}
