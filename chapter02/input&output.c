/*
输入和输出
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char const *argv[])
{
    
    //1.使用scanf输入
    /*
    int num;
    float price;
    char c;
    char str[10];
    
    scanf("%d",&num);
    scanf("%f",&price);//注意：'\n'会滞留在缓冲区
    scanf(" %c",&c);
    scanf("%s",&str);
    
    printf("\n-----------output result--------------\n");
    printf("%d\n",num);
    printf("%f\n",price);
    printf("%c\n",c);
    printf("%s\n",str);
    */

    //2.getchar()接收单个字符

    /*
    float price;
    char c1;
    char c2;
    
    scanf("%f",&price);
    scanf("%c",&c1);
    scanf("%f",&price);
    getchar(); //接收缓冲区里的'\n' 
    c2=getchar();
    printf("\n--------output result-----------\n");
    printf("%f\n",price);
    printf("%c\n",c1);
    printf("%c\n",c2);
    */

    //3.gets()专用的字符串输入函数
     
     
    char str1[10];
    char str2[10];
   

    scanf("%s",str1);
    getchar();
    gets(str2);
    printf("\n-------output result-----------\n");
    printf("%s\n",str1);
    printf("%s\n",str2);
    return 0;
}
