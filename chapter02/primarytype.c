/*
C语言的基本数据类型。
*/

#include <stdio.h>
enum DIRECTION
{
    NORTH,
    SOUTH,
    WEST,
    EAST
};
int main(int argc, char const *argv[])
{
    char c = 'A';
    short s = 127;
    int n = 1000;
    long l = 50000;
    float f = 3.14;
    double pi = 3.1415926;

    enum DIRECTION d;
    d = NORTH;

    switch (d)
    {
    case NORTH:
        printf("%s\r\n", "北");
        break;
    case SOUTH:
        printf("%s\r\n", "南");
        break;
    case WEST:
        printf("%s\r\n", "西");
        break;
    case EAST:
        printf("%s\r\n", "东");
        break;
    }

    printf("%c\r\n", c);
    printf("%d\r\n", s); ///*%d是输出控制符，d 表示十进制，后面的 s 是输出参数*/
    printf("%d\r\n", n);
    printf("%ld\r\n", l);
    printf("%f\r\n", f);
    printf("%f\r\n", pi);

    return 0;
}
