/*
小数类型
*/

#include <stdio.h>
#include <float.h>

int main(int argc, char const *argv[])
{
    int float_length = sizeof(float);
    int double_length = sizeof(double);
    
    printf("float length=%d, double length=%d\n", float_length, double_length);

    printf("max float = %f\n",FLT_MAX);
    printf("min float = %f\n",FLT_MIN);
    printf("max double = %lf\n",DBL_MAX);
    printf("min double = %lf\n",DBL_MIN);

    
    /*
    long a = 100;  //默认把 int 转换为long 
    int b = 294;  
    float x = 52.55;  //默认把double 转换为float 
    double y = 18.6;
	
    printf("100 length is %d\n",sizeof(100));
    printf("a length is %d\n",sizeof(a));
    printf("52.55 length is %d\n",sizeof(52.55));
    printf("x length is %d\n",sizeof(x));
    */


    long a = 100L;  //默认把 int 转换为long 
    int b = 294;  
    float x = 52.55f;  //默认把double 转换为float 
    double y = 18.6;
	
    printf("100L length is %d\n",sizeof(100L));
    printf("a length is %d\n",sizeof(a));
    printf("52.55f length is %d\n",sizeof(52.55f));
    printf("x length is %d\n",sizeof(x));


    printf("max float = %f\n",FLT_MAX);
    printf("max double = %lf\n",DBL_MAX);
    printf("max float = %e\n",FLT_MAX);
    printf("min double = %le\n",DBL_MAX);
    printf("max float = %E\n",FLT_MAX);
    printf("min double = %LE\n",DBL_MAX);

    return 0;
}
