#include <stdio.h>
#include <stdlib.h>
#define N 500 //每一行最多读取500个字符

int main(int argc, char const *argv[])
{
    FILE *fin;
    FILE *fout;
    char str[N + 1];

    //判断文件是否打开失败
    if ((fin = fopen("e:\\test\\test.c", "rt")) == NULL)
    {
        puts("打开文件失败!");
        exit(0);
    }
    else
    {
        //创建输出文件
        fout = fopen("e:\\test\\dest.c", "wt");
    }
    //循环读取文件的每一行数据
    while (fgets(str, N, fin) != NULL)
    {
        printf("%s", str);
        fputs(str, fout);
    }

    //操作结束后关闭文件
    fclose(fin);
    fclose(fout);
    return 0;
}
