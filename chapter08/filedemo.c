#include <stdio.h>
#include <stdlib.h>
#include <windows.h>

int main(int argc, char const *argv[])
{
    FILE *fp;

    if ((fp = fopen("e:\\test\\test.c", "rt")) == NULL)
    {
        puts("文件打开失败!");
        exit(0);
    }
    else
    {
        puts("文件打开成功!");
    }

    Sleep(3000);

    if (fclose(fp) == 0)
    {
        puts("文件正常关闭！");
    }
    else
    {
        puts("文件关闭异常！");
    }
    return 0;
}
