#include <stdio.h>
#include <stdlib.h>
#define N 1024 //定义缓冲区的大小

int main(int argc, char const *argv[])
{
    int buff[N];
    int size = sizeof(int);
    FILE *fin;
    FILE *fout;
    if ((fin = fopen("e:\\test\\C罗.jpg", "rb+")) == NULL)
    { //以二进制方式打开
        puts("打开文件失败!");
        exit(0);
    }
    else
    {
        //创建输出文件
        fout = fopen("e:\\test\\dest.jpg", "wb+");
    }

    int len = -1; //每次读出来的字节长度
    len = fread(buff, size, N, fin);

    while (1)
    {
        printf("len is:%d\n", len);
        //把buff内容写入到目标文件,读出多少个字节就写入多少个字节
        fwrite(buff, size, len, fout);
        if (len != N)
        { //说明读到了文件的结尾
            break;
        }
        len = fread(buff, size, N, fin);
    }

    printf("\n-------file copy end!---------\n");
    fclose(fin);
    fclose(fout);
    return 0;
}
