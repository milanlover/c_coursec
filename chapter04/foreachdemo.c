#include <stdio.h>
#include <stdlib.h>
#define LENTH 10

int main(int argc, char const *argv[])
{
    int arr[LENTH];
    for (int i = 0; i < LENTH; i++)
    {
        printf("请输入第%d个元素:", i + 1);
        scanf("%d", &arr[i]);
    }
    for (int i = 0; i < LENTH; i++)
    {
        printf("%d ", arr[i]);
    }
    return 0;
}
