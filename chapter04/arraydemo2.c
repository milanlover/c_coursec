#include <stdio.h>

int main(int argc, char const *argv[])
{
    int arr[5] = {1};
    for (int i = 0; i < 5; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n数组占用字节数：%d\n", sizeof(arr));
    printf("\n数组的元素个数：%d\n", sizeof(arr) / sizeof(int));
    // printf("\n数组的元素个数：%d\n",sizeof(arr)/sizeof(arr[0]));
    // c语言不会检查数组下标越界
    printf("arr第六个元素是：%d", arr[5]);
    return 0;
}
