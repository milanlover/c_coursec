#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const *argv[])
{
    char str1[6] = {'h', 'e', 'l', 'l', 'o', '\0'}; //指定长度
    char str2[] = {'h', 'e', 'l', 'l', 'o', '\0'};  //不指定长度
    char str3[6] = "hello";                         //字符串直接赋给数组
    char str4[6] = {"hello"};                       //符串直接赋给数组
    char str5[] = "hello";                          // 指定长度的字符串直接赋给数组
    char *str6 = "hello";                           //使用字符指针

    printf("\n---------out with printf----------------\n");
    printf("%s\n", str1);
    printf("%s\n", str2);
    printf("%s\n", str3);
    printf("%s\n", str4);
    printf("%s\n", str5);
    printf("%s\n", str6);

    printf("\n---------out with puts----------------\n");
    puts(str1);
    puts(str2);
    puts(str3);
    puts(str4);
    puts(str5);
    puts(str6);

    return 0;
}
