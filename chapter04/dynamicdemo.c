#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const *argv[])
{
    int arr1[] = {1, 2, 3, 4, 5};
    int *arr2 = (int *)malloc(5 * sizeof(int));

    for (int i = 0; i < 5; i++)
    {
        arr2[i] = i + 1;
        printf("%d ", arr2[i]);
    }
    printf("\n----------\n");
    printf("\n静态初始化的数组arr1占用字节数量：%d\n", sizeof(arr1));
    printf("\n指针变量arr2占用的字节数量：%d\n", sizeof(*arr2));
    printf("\n指针变量arr2指向数组第一个元素值是：%d\n", *arr2);
    printf("\n指针变量arr2里保存的地址值的数据类型占用字节数量：%d\n", sizeof(arr2));
    printf("\n指针变量arr2里保存的地址值是：%d\n", arr2);
    return 0;
}
