#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char const *argv[])
{
    char str1[] = "我爱你中国";
    char str2[] = "我爱你中国";
    char str3[] = " I am a chinese!";
    char str4[] = "I love you china";

    printf("str1 的长度是：%d\n", strlen(str1));
    printf("str2 的长度是：%d\n", strlen(str2));

    if (strcmp(str1, str2) == 0)
    {
        printf("str1 与 str2 内容相同\n");
    }
    else
    {
        printf("str1 与 str2 内容不同\n");
    }

    strcat(str1, str3); //将str2拼接在str1的后面
    puts(str1);

    strcpy(str2, str4);
    puts(str2);

    if (strcmp(str1, str2) == 0)
    {
        printf("str1 与 str2 内容相同\n");
    }
    else
    {
        printf("str1 与 str2 内容不同\n");
    }
    return 0;
}
