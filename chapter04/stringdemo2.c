#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const *argv[])
{
    char str1[6];
    // str1="hello"; //错误！
    str1[0] = 'h';
    str1[1] = 'e';
    str1[2] = 'l';
    str1[3] = 'l';
    str1[4] = 'o';
    str1[5] = '\0';
    puts(str1);
    return 0;
}
