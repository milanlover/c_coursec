#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char const *argv[])
{
    char str1[30] = "abcdefghijklmnopqrstuvwxyz";
    char str2[30];
    char str3[30] = {0};
    for (int j = 0, i = 97; i < 123; j++, i++)
    {
        str2[j] = i;
        str3[j] = i;
    }
    puts(str1);
    puts(str2);
    puts(str3);
    return 0;
}
