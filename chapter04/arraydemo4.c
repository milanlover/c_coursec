#include <stdio.h>
#define LEN(x) sizeof(x) / sizeof(x[0])

int main(int argc, char const *argv[])
{
    int arr[] = {1, 2, 3, 4, 5, 100};
    int len = LEN(arr);
    printf("%d\n", len);
    for (int i = 0; i < len; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n数组占用字节数：%d\n", sizeof(arr));
    printf("\n数组的元素个数：%d\n", sizeof(arr) / sizeof(arr[0]));
    // c语言不会检查数组下标越界
    printf("arr第六个元素是：%d", arr[5]);
    return 0;
}
