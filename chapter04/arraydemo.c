#include <stdio.h>

int main(int argc, char const *argv[])
{
    int arr[5] = {1, 2, 3, 4, 5};
    for (int i = 0; i < 5; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("\n数组占用字节数：%d\n", sizeof(arr));
    // c语言不会检查数组下标越界
    printf("arr第六个元素是：%d", arr[5]);
    return 0;
}
