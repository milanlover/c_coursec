#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char const *argv[])
{
    char str1[] = {'h', 'e', 'l', 'l', 'o'}; //逐个字符地给数组赋值并不会自动添加'\0'
    char str2[] = "hello";                   //整体赋给字符串结尾默认添加'\0'

    puts(str1);
    puts(str2);

    printf("str1 length is:%d\n", strlen(str1)); //注意：strlen方法不计算结束标识符
    printf("str2 length is:%d\n", strlen(str2));

    printf("str1 size is:%d\n", sizeof(str1));
    printf("str2 size is:%d\n", sizeof(str2));
    return 0;
}
