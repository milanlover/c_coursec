#include <stdio.h>
//用宏定义define
#define MAXSIZE 10
#define LEN(x) sizeof(x) / sizeof(x[0])

int main(int argc, char const *argv[])
{
    const int size = 10 ;
    int arr1[MAXSIZE];
    int arr2[size];

    printf("arr1 length is:%d\n", LEN(arr1));
    printf("arr2 length is:%d\n", LEN(arr2));

    return 0;
}
