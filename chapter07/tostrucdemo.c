#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SCORE_LEN 5
#define SCHOOL_LEN 50

typedef struct
{
    char *name;
    int age;
    int score[SCORE_LEN];
    char school[SCHOOL_LEN];
} Students;

int main(int argc, char const *argv[])
{
    Students s;
    Students *pt;

    s.name = "张三";
    s.age = 18;
    int arr[] = {76, 80, 77, 84, 90};
    memcpy(s.score, arr, sizeof(int) * SCORE_LEN);
    strcpy(s.school, "西安电子科技大学");

    printf("姓名:%s\n", s.name);
    printf("年龄:%d\n", s.age);
    printf("------考试分数------\n");
    for (int i = 0; i < SCORE_LEN; i++)
    {
        printf("%d ", s.score[i]);
    }
    printf("\n学校：%s\n", s.school);
    printf("\n----------------\n");
    pt = &s;
    printf("姓名:%s\n", pt->name);
    printf("年龄:%d\n", pt->age);
    printf("------考试分数------\n");
    for (int i = 0; i < SCORE_LEN; i++)
    {
        printf("%d ", pt->score[i]);
    }
    printf("\n学校：%s\n", pt->school);

    printf("\n----------------\n");
    printf("姓名:%s\n", (*pt).name);
    printf("年龄:%d\n", (*pt).age);
    printf("------考试分数------\n");
    for (int i = 0; i < SCORE_LEN; i++)
    {
        printf("%d ", (*pt).score[i]);
    }
    printf("\n学校：%s\n", (*pt).school);
    return 0;
}
