#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SCORE_LEN 5
#define SCHOOL_LEN 50

int main(int argc, char const *argv[])
{
    struct Students
    {
        char *name;              //姓名
        int age;                 //年龄
        int score[SCORE_LEN];    //考试分数
        char school[SCHOOL_LEN]; //学校名称
    } s;

    s.name = "张三";
    s.age = 18;
    int arr[] = {76, 80, 77, 84, 90};
    memcpy(s.score, arr, sizeof(int) * SCORE_LEN); //数组拷贝
    strcpy(s.school, "西安电子科技大学");          //字符串拷贝

    printf("姓名:%s\n", s.name);
    printf("年龄:%d\n", s.age);
    printf("------考试分数------\n");
    for (int i = 0; i < SCORE_LEN; i++)
    {
        printf("%d ", s.score[i]);
    }
    printf("\n学校：%s\n", s.school);
    return 0;
}
