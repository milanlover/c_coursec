#include <stdio.h>
//枚举值就从 1 开始递增
enum Week
{
    Mon = 1,
    Tues,
    Wed,
    Thurs,
    Fri,
    Sat,
    Sun
};

int main(int argc, char const *argv[])
{
    enum Week weekend;
    scanf("%d", &weekend);
    switch (weekend)
    {
    case Mon:
        puts("Monday");
        break;
    case Tues:
        puts("Tuesday");
        break;
    case Wed:
        puts("Wednesday");
        break;
    case Thurs:
        puts("Thursday");
        break;
    case Fri:
        puts("Friday");
        break;
    case Sat:
        puts("Saturday");
        break;
    case Sun:
        puts("Sunday");
        break;
    default:
        puts("Error!");
    }
    return 0;
}
