#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define SCORE_LEN 5
#define SCHOOL_LEN 50
#define STUDENTS_LEN 5

typedef struct
{
    char *name;
    int age;
    int score[SCORE_LEN];
    char school[SCHOOL_LEN];
} Students;

void printStudentsInfo(Students s)
{
    printf("姓名：%s\n", s.name);
    printf("年龄：%d\n", s.age);
    printf("------考试分数------\n");
    for (int i = 0; i < SCORE_LEN; i++)
    {
        printf("%d ", s.score[i]);
    }
    printf("\n学校：%s\n", s.school);
}

int main(int argc, char const *argv[])
{
    Students stus[STUDENTS_LEN] = {
        {"张三", 18, {65, 77, 80, 92, 73}, "西安电子科技大学"},
        {"李四", 17, {60, 77, 82, 96, 73}, "西安交通大学"},
        {"王五", 19, {65, 87, 80, 72, 71}, "西北工业大学"},
        {"赵六", 20, {75, 77, 97, 92, 63}, "西北大学"},
        {"孙琦", 21, {68, 79, 88, 90, 53}, "西安邮电大学"},
    };

    Students *pt = stus;

    for (int i = 0; i < STUDENTS_LEN; i++)
    {
        printStudentsInfo(stus[i]);
    }

    printf("\n--------------------------\n");
    for (int i = 0; i < STUDENTS_LEN; i++)
    {
        printStudentsInfo(*pt);
        pt++;
    }
    return 0;
}
